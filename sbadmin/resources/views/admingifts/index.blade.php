@extends('layouts.app')

@section('title') Add Gifts @stop

@section('main-content')
<div class="container spark-screen">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Add Gift</div>

                <div class="panel-body">
                    <div class='col-lg-4 col-lg-offset-4'>

                        @if ($errors->has())
                        @foreach ($errors->all() as $error)
                        <div class='bg-danger alert'>{{ $error }}</div>
                        @endforeach
                        @endif

                        <h1><i class='fa fa-user'></i> Add Gift</h1>

                        {{ Form::open(['role' => 'form', 'url' => '/addgift']) }}

                        <div class='form-group'>
                            {{ Form::label('message', 'Message') }}
                            {{ Form::text('message', null, ['class' => 'form-control']) }}
                        </div>

                        <div class='form-group'>
                            {{ Form::label('deviceid', 'Device Id') }}
                            {{ Form::text('deviceid', null, ['class' => 'form-control']) }}
                        </div>

                        <div class='form-group'>
                            {{ Form::label('coins', 'Coins') }}
                            {{ Form::text('coins', null, ['class' => 'form-control']) }}
                        </div>

                        <div class='form-group'>
                            {{ Form::label('gems', 'Gems') }}
                            {{ Form::text('gems', null, ['class' => 'form-control']) }}
                        </div>

                        <div class='form-group'>
                            {{ Form::label('levels', 'Level') }}
                            {{ Form::text('levels', null, ['class' => 'form-control']) }}
                        </div>

                        <div class='form-group'>
                            {{ Form::label('password', 'Password') }}
                            {{ Form::text('password', null, ['class' => 'form-control']) }}
                        </div>

                        <div class='form-group'>
                            {{ Form::submit('Send', ['class' => 'btn btn-primary']) }}
                        </div>

                        {{ Form::close() }}

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
