<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdmingiftsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admingifts', function (Blueprint $table) {
            $table->integer('ip');
            $table->integer('coins');
            $table->integer('gems');
            $table->boolean('taken');
            $table->integer('fbid');
            $table->string('device_id');
            $table->string('message');
            $table->integer('spending_gift');
            $table->integer('level_gift');
            $table->string('source');
            //$table->datetime('time_created');
            $table->timestamps();
        });//
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('admingifts');//
    }
}
