<?php

namespace App\Http\Controllers;

use App\AdminGift;
use Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use GuzzleHttp\CLient;

use App\Http\Requests;

class AdmingiftsController extends Controller
{
    public function index() {
        return View::make('admingifts.index');
    }

    public function store() {
        $client = new Client();
        $res = $client->request('POST', 'localhost/submitaddgift.php', [
            'form_params' => [
                'coins' => Input::get('coins'),
                'gems' => Input::get('gems'),
                'deviceid' => Input::get('deviceid'),
                'messagetext' => Input::get('message'),
                'password' => Input::get('password'),
                'levels' => Input::get('levels')
                ]
            ]);
        echo $res->getStatusCode();
        // "200"
        //echo $res->getHeader('content-type');
        // 'application/json; charset=utf8'
        echo $res->getBody();
        // {"type":"User"...'

    }

    public function process() {
        $gift = new AdminGift;

        $gift->coins = Input::get('coins');
        $gift->gems = Input::get('gems');
        $gift->device_id = Input::get('deviceid');
        $gift->message = Input::get('message');
        $gift->level_gift = Input::get('levels');
        $gift->ip = 444;
        $gift->taken = false;
        $gift->fbid = 222;
        $gift->source = 'ADMIN';
        //$gift->time_created = Carbon::now();

        $gift->save();
        return Redirect::to('/home');


    }//
}
